# Simple Express App: pizza-express
Source repo: https://github.com/turingschool-examples/pizza-express

Parental repo: https://github.com/OrenAshkenazyBigID/pizza-express



## How to build
**Requirements:** Docker Community Edition

To start the app run: `docker-compose up`

It will then be started on port 3000.



## Helpful links
YAML validator: http://www.yamllint.com



## Docker build
Image is located here: https://hub.docker.com/repository/docker/dspv1/pizza-express-oren

Run:
`docker push dspv1/pizza-express-oren:tagname`
