FROM node:alpine

# Create app dir
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# If building for production
# RUN npm ci --only=production

# App source
COPY . .

EXPOSE 3000
EXPOSE 6379

CMD [ "node", "server.js" ]
